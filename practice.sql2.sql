create database textbook_sample DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
use textbook_sample;

create table m_item(
	item_id varchar(10) primary key,
	item_nm varchar(100),
	item_price int
);
create table m_customer(
	cust_id varchar(10) primary key,
	cust_nm varchar(100),
	cust_age int,
	cust_tel varchar(20)
);
create table t_sale(
    sale_id varchar(10) primary key,
    sale_date date,
    cust_id varchar(10)
);

-- 伝票詳細テーブル
create table t_sale_detail(
    sale_id varchar(10),
    item_id varchar(10),
    item_count int,
    primary key (
        sale_id,
        item_id
    )
);


-- 商品マスタ insert情報
insert into m_item(item_id,item_nm,item_price) values ('0001','鉛筆',80);
insert into m_item(item_id,item_nm,item_price) values ('0002','ノート',120);
insert into m_item(item_id,item_nm,item_price) values ('0003','はさみ',320);
insert into m_item(item_id,item_nm,item_price) values ('0004','電池',320);
insert into m_item(item_id,item_nm,item_price) values ('0005','メモリ',8900);
insert into m_item(item_id,item_nm,item_price) values ('0006','ティッシュ',240);

-- 顧客マスタ insert情報
insert into m_customer(cust_id,cust_nm,cust_age,cust_tel) values ('0001','佐藤',22,'0311112222');
insert into m_customer(cust_id,cust_nm,cust_age,cust_tel) values ('0002','鈴木',26,'0322223333');
insert into m_customer(cust_id,cust_nm,cust_age,cust_tel) values ('0003','田中',46,'0344445555');

-- 伝票テーブル insert情報
insert into t_sale(sale_id,sale_date,cust_id) values ('0001','2019-05-05','0001');
insert into t_sale(sale_id,sale_date,cust_id) values ('0002','2019-05-05','0002');
insert into t_sale(sale_id,sale_date,cust_id) values ('0003','2019-08-08','0003');
insert into t_sale(sale_id,sale_date,cust_id) values ('0004','2019-12-12','0001');

-- 伝票詳細テーブル insert情報
insert into t_sale_detail(sale_id,item_id,item_count) values('0001','0001',10);
insert into t_sale_detail(sale_id,item_id,item_count) values('0001','0002',2);
insert into t_sale_detail(sale_id,item_id,item_count) values('0001','0003',1);
insert into t_sale_detail(sale_id,item_id,item_count) values('0002','0004',4);
insert into t_sale_detail(sale_id,item_id,item_count) values('0002','0005',1);
insert into t_sale_detail(sale_id,item_id,item_count) values('0003','0003',2);
insert into t_sale_detail(sale_id,item_id,item_count) values('0004','0006',2);

